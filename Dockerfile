FROM node:6-alpine
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY emp .
RUN npm install
EXPOSE 3000
CMD [ "node", "index.js" ]