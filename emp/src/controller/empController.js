const aws = require("./../lib/interface/aws.js");
const database = require("./../lib/helper/database.js");
class empController {
    constructor() {
        this.aws = new aws();
        this.db = new database();
        this.tableName = "ExecutionPlan";
    }

    /*
    {
        "operator": "operator1",
        "executionPlan": {"operator1": {
        "order": [
            "order1",
            "order2"
        ],
        "event": [
            "event1",
            "event2"
        ]
    }}
    }
     */

    getEmp(req, res) {
        this.db.fetchData(this.tableName, (err, result) => {
            res.send(result);
        });
    }

    getEmpById(req, res) {
        this.db.queryData(this.tableName, {key: "operator", value: req.params.id}, (err, result) => {
            res.send(result);
        });
    }

    addEmp(req, res) {
        var values = req.body;

        this.db.addData(this.tableName, values,(err, result) => {
            res.send(result);
        })
    }

    updateEmp(req, res) {
        console.log(req);
        var queryObj = {operator: req.body.operator, operatorName: req.body.operatorName};
        var updateExpression = "set  #e = :e";
        var expressionAttribute = {
            ":e": req.body.executionPlan
        }

        var expressionAttributeName = {
            "#e": "executionPlan",
        }

        this.db.updateData(this.tableName, queryObj, updateExpression, expressionAttribute, expressionAttributeName,(err, result) => {
            res.send(result);
        })
    }

}

module.exports = empController;