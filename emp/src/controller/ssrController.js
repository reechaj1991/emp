const request = require("./../lib/interface/request.js");

class ssrController {
    constructor() {
        this.request = new request();
    }

    generateExecutionPlan(req, res) {
        // input: operator, order [], events
        var operator = req.body.operator;
        var operatorId = req.body.operatorId;
        var order = req.body.order;
        var event = req.body.event;

        var executionPlan = this.generateExecutionPlanObj(operatorId, operator, order, event)

        res.send(executionPlan);
    }

    addExecutionPlan(req, res) {
        var operator = req.body.operator;
        var operatorId = req.body.operatorId;
        var order = req.body.order;
        var event = req.body.event;

        this.request.performNetworkCall("http://localhost:3000/operator/"+operatorId, "GET", null, (data) => {
            var responseObj = JSON.parse(data);
            if (responseObj.length != 0) {
                var responseData = responseObj[0];
                var existingOrder = responseData.executionPlan.order;
                var existingEvent = responseData.executionPlan.event;

                order.map((val) => {
                    if (!(existingOrder.includes(val))) {
                        existingOrder.push(val)
                    }
                });

                event.map((val) => {
                    if (!(existingEvent.includes(val))) {
                        existingEvent.push(val)
                    }
                });

                var executionPlan = this.generateExecutionPlanObj(operatorId, operator, existingOrder, existingEvent);
                console.log(executionPlan);
                this.request.performNetworkCall("http://localhost:3000/emps", "PUT", executionPlan, (data) => {
                    res.send(data);
                });
            } else {
                var executionPlan = this.generateExecutionPlanObj(operatorId, operator, order, event);
                this.request.performNetworkCall("http://localhost:3000/emp", "POST", executionPlan, (data) => {
                    res.send(data);
                });
            }
        });
    }

    generateExecutionPlanObj (operatorId, operator, order, event){
        return {
            operator: operatorId,
            operatorName: operator,
            executionPlan: {
                operator,
                order,
                event
            }
        }
    }
}

module.exports = ssrController;