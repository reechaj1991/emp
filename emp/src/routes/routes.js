const empController = require("./../controller/empController.js");
const ssrController = require("./../controller/ssrController.js");

class Routes {
    constructor() {

    }
    initRoutes(app) {
        app.get('/', function(req, res){
            res.send("working here");
        });

        var empc = new empController();
        app.get("/emp", empc.getEmp.bind(empc));
        app.get("/emp/:id", empc.getEmpById.bind(empc));
        app.post("/emp", empc.addEmp.bind(empc));
        app.put("/emp", empc.updateEmp.bind(empc))

        var ssrc = new ssrController();
        app.post("/ssr/generate", ssrc.generateExecutionPlan.bind(ssrc));
        app.post("/ssr/add", ssrc.addExecutionPlan.bind(ssrc));
    }
}

module.exports = new Routes();