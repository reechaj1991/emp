const aws = require("./../interface/aws.js");

class Database {
    constructor() {
        var awsObj = new aws();
        this.db = awsObj.fetchAwsObject("db");
    }

    fetchData(table, callback) {
        var params = {
            TableName : table
        };

        this.db.scan(params, function(err, data) {
            if (err) {
                console.log(err);
                callback(err)
            } else {
                callback(null, data);
            }
        });
    }

    queryData(table, queryObj, callback) {

        var params = {
            TableName: table,
            KeyConditionExpression: "#key = :value",
            ExpressionAttributeNames:{
                "#key": queryObj.key
            },
            ExpressionAttributeValues: {
                ":value":queryObj.value
            }
        };

        this.db.query(params, function(err, data) {
            if (err) {
                console.log(err);
                callback(err)
            } else {
                callback(null, data.Items);
            }
        });
    }

    addData(table, value, callback) {
        var params = {
            TableName: table,
            Item: value
        }

        this.db.put(params, function(err, data) {
            if (err) {
                console.log(err);
                callback(err)
            } else {
                callback(null, data);
            }
        });
    }

    updateData(table, queryObj, updateExpression, expressionAttribute, expressionAttributeName, callback) {
        var params = {
            TableName:table,
            Key:queryObj,
            UpdateExpression: updateExpression,
            ExpressionAttributeValues: expressionAttribute,
            ExpressionAttributeNames: expressionAttributeName,
            ReturnValues:"UPDATED_NEW"
        };

        this.db.update(params, function(err, data) {
            if (err) {
                console.log(err);
                callback(err)
            } else {
                callback(null, data);
            }
        });
    }

    createTable(table, schema, attribute, callback) {
        var params = {
            TableName: table,
            // Item: {
            //     HashKey: 'haskey',
            //     NumAttribute: 1,
            //     BoolAttribute: true,
            //     ListAttribute: [1, 'two', false],
            //     MapAttribute: { foo: 'bar'},
            //     NullAttribute: null
            // }
            KeySchema: schema,
            AttributeDefinitions: attribute
        };

        this.db.createTable(params, function(err, data) {
            if (err) {
                callback(err);
            } else {
                callback(null, data);
            }
        });
    }
}

module.exports = Database;