const awsSdk = require('aws-sdk');
class AWS {
    constructor() {
        awsSdk.config.update({
            accessKeyId: process.env.AWS_ACCESSKEY_ID,
            secretAccessKey: process.env.AWS_ACCESSKEY_SECRET,
            region: "eu-west-1"
        });

        this.s3 = new awsSdk.S3({apiVersion: '2006-03-01'});
        this.db = new awsSdk.DynamoDB.DocumentClient();

        this.awsObject = {
            "s3": this.s3,
            "db": this.db
        }
    }

    fetchAwsObject(key) {
        return this.awsObject[key];
    }

    listBuckets(callback){
        this.s3.listBuckets(function(err, data) {
            if (err) {
                callback(err);
            } else {
                console.log("My buckets now are:\n");

                var bucketList = [];
                for (var i = 0; i < data.Buckets.length; i++) {
                    bucketList.push(data.Buckets[i].Name);
                }

                callback(bucketList);
            }
        });
    }

}

module.exports = AWS;