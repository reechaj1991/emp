const request = new require("request");

class Request {
    constructor() {

    }
    performNetworkCall(url, method, data, callback) {
        var requestObj = {
            url: url,
            method: method
        }

        if (method != "GET") {
            requestObj["body"] = JSON.stringify(data);
            requestObj["headers"] = {
                'Content-Type':'application/json'
            }
        }

        request(requestObj, function (error, response, body) {
            if (error) {
                callback(error)
            } else {
                callback(body);
            }
        });
    }

}

module.exports = Request;